<?php
/**
 * Created by PhpStorm.
 * User: mirzat
 * Date: 2018/10/9
 * Time: 上午8:49
 */

namespace App\Services;

use App\Repositories\UserRepository;

class UserService extends BaseService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getUser(int $userId)
    {
        return $this->userRepository->getUser($userId);
    }
}
