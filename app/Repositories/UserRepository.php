<?php
/**
 * Created by PhpStorm.
 * User: mirzat
 * Date: 2018/10/9
 * Time: 上午8:46
 */

namespace App\Repositories;

use App\Entities\User;

class UserRepository extends BaseRepository
{
    public function getUser(int $userId)
    {
        $user = (new User())->find($userId);
        return $user->toArray();
    }
}
