<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function get(int $userId)
    {
        return $this->userService->getUser($userId);
    }

    public function update(Request $request, $userId)
    {
        $param = $this->parseRequest($request);

        $this->userService->updateUser($param, $userId);
    }
}
