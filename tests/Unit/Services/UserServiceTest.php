<?php
/**
 * Created by PhpStorm.
 * User: mirzat
 * Date: 2018/10/11
 * Time: 下午9:05
 */

namespace Tests\Unit\Services;


use App\Entities\User;
use App\Repositories\UserRepository;
use App\Services\UserService;
use Tests\TestCase;

class UserServiceTest extends TestCase
{

    protected $userService ;
    protected $userRepository;
    public function setUp()
    {
        parent::setUp();
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->userService = new UserService($this->userRepository);
    }


    public function testGetUser_success()
    {
        $userData = [
            'name' => 'mirza',
            'email' => 'mirzat@abc.com',
            'password' => bcrypt('mirzat'),
        ];

        $this->userRepository
            ->method('getUser')
            ->willReturn($userData);
        $user = $this->userService->getUser(1);

        $this->assertEquals($userData, $user);
    }
}