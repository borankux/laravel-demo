<?php
/**
 * Created by PhpStorm.
 * User: mirzat
 * Date: 2018/10/9
 * Time: 上午8:55
 */

namespace Tests\Feature\Users;


use App\Entities\User;
use Tests\Feature\BaseFeatureTest;
use Tests\TestCase;

class UserControllerTest extends BaseFeatureTest
{
    public function testGetUser_success()
    {

        $user = new User();
        $user->name = 'mirzat';
        $user->email = 'mirzat@abc.com';
        $user->password = bcrypt('mirzat');

        $user->save();

        $response = $this->get('/api/user/'.$user->id);
        $got = json_decode($response->getContent())->name;
        $this->assertEquals('mirzat', $got);
    }

    public function testGetUser_not_existed()
    {
        $response = $this->get('/api/user/d9a8hd98');
        $got = json_decode($response->getContent());
        $this->assertNull($got);
    }


}